# htmltopng

#### 介绍
Springboot后台HTML/富文本转图片。
我对接了其中三种技术：html2image、cssbox和第三方工具wkhtmltox。

项目场景：
项目场景：后台将html文件转换成图片资源。项目是老师布置作业给学生做，作业试题对接的是第三方题库的接口，但它们都采用题干为HTML格式，而我们传统的试题格式都是图片格式，则需要调用第三方题库后，将每个试题的题干富文本存入到HTML文件中，并引用第三方题库的富文本css样式适配题干原有的展示样式，最后拿整个HTML文件去转对应试题的图片。

实现描述
提示：这里描述可以实现转换的技术


我对接了其中三种技术：html2image、cssbox和第三方工具wkhtmltox。
做个总结：  
html2images：实现起来非常简单，只需要引用html2image的pom依赖，实现其核心类HtmlImageGenerator的loadHtml()方法，就可以实现转换。
优：实现简单，易用；
缺：a.如果html代码里带有图片时，生成的程序必须有一定的等待时间（Thread.sleep(xx)），等待时间需要根据实际图片大小去设置（初步估计是工具加载流未完成就进行图片的生成以及保存工作）
   b.画布背景颜色变红，可重新设置画布背景颜色。 
   c.对Css3样式支持很差（width标签设置都不生效），复杂页面不友好。
个人评价：技术门槛低，十分简单html页面转换时使用。

cssbox：实现起来也比较简单，引用cssbox的pom依赖，将html代码文本创建一个cssbox文档资源DocumentSource，再通过cssbox的DOMSource对其文档解析并通过cssbox的DOMAnalyzer创建CSS解析器设置样式属性，最后通过创建BrowserCanvas浏览器画布设置其宽高尺寸得到图像。
相比于html2images，cssbox解决了html代码里有图片时的问题和画布背景颜色变红问题，但还是对Css3样式支持不是太理想（width标签设置还是不生效），仅支持它内部实现的CSS解析器。
个人评价：通过画布的原理来实现的，未最终采用它的原因是width标签设置不生效。

wkhtmltox：通过第三方工具包来实现转换，不需要知道如何实现，只需要知道如何安装。
解决了我上述所提到的几个问题abc，但安装麻烦。（window上安装及其简单；Linux物理机上直接跑项目的时候安装也不复杂，但Linux用docker安装wkhtmltox并给docker部署SpringBoot项目的容器中调用wkhtmltox容器转换工具时，部署比较复杂）