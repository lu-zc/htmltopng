## 说明

wkhtmltox 实现HTML代码生成PDF文档或者图片
参考文章：https://www.cnblogs.com/cksvsaaa/p/6125546.html

Windows环境安装：wkhtmltox-0.12.6-1.msvc2015-win64.exe  
Linux物理机环境安装：wkhtmltox-0.12.6-1.centos7.x86_64.rpm

我们Linux环境采用docker部署SpringBoot项目，所以略有不同
参考文章：https://blog.csdn.net/weixin_42838675/article/details/116023685
1. 拉取wkhtmltopdf镜像：  
   ```docker pull dicoming/wkhtmltopdf-ws:latest```
2. 运行镜像  
   ```docker run -v /tmp/wkhtmltopdf:/tmp/wkhtmltopdf -v /usr/share/fonts:/usr/share/fonts -p 10080:80 --name="wkhtmltopdf" -d dicoming/wkhtmltopdf-ws```  
   说明：  
   1. -v /tmp/wkhtmltopdf:/tmp/wkhtmltopdf：wkhtmltox工具的tmp数据，同teaching-business模块
   中docker-compose.yml中的volumes: - /tmp/wkhtmltopdf:/tmp/wkhtmltopdf做了映射关系
   2. -v /usr/share/fonts:/usr/share/fonts：wkhtmltox工具需要的字体同物理机做映射关系
   3. -p 10080:80 ：wkhtmltox的docker容器80映射10080（10080需要开端口）![img.png](img.png)