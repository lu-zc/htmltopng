package com.demo.htmltopng.service;

/**
 * @author AnYuan
 * 生成png服务接口
 */
public interface CssBoxService {

    void htmlToPng() throws Exception;
}
