package com.demo.htmltopng.service.impl;

import com.demo.htmltopng.service.CssBoxService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * CssBox测试类
 * @author luzck
 * @since 2022/7/2
 */
@SpringBootTest
public class CssBoxTest {

    @Resource
    private CssBoxService cssBoxService;

    @Test
    public void test() throws Exception {

        cssBoxService.htmlToPng();
    }

}
