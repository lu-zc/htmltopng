package com.demo.htmltopng.service.impl;

import com.demo.htmltopng.service.CssBoxService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
class WkHtmlToxTest {

    @Autowired
    private CssBoxService cssBoxService;

    @Autowired
    private WkHtmlToxServiceImpl wkHtmlToPngService;

    @Test
    public void htmlToPngTest(){
        try {
            wkHtmlToPngService.wkHtmlToPng();
        }catch (Exception e) {
            log.info("PngError:{}", e.getMessage());
        }
    }
}