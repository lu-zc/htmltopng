package com.demo.htmltopng.service.impl;

import com.demo.htmltopng.util.Html2ImageUtil;
import org.junit.jupiter.api.Test;

/**
 * html2image测试类
 * @author luzck
 * @since 2022/7/2
 */
public class Html2ImageTest {

    @Test
    public void test() throws InterruptedException {
        String htmlTemplate = "<html><head><title>Welcome!</title></head><body style=\"margin:0px\"><h1>搬砖码农 辛苦了!</h1><img src=\"https://pic.cnblogs.com/avatar/2319511/20210304170859.png\"></body></html>";
        Html2ImageUtil.htmlToImage(htmlTemplate);
    }

}
